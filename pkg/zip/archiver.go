package zip

import (
	"archive/zip"
	"bytes"
	"gitlab.com/gogoapps/kniepok/pictures"
	"io"
)

type Archiver struct {
}

func NewArchiver() *Archiver {
	return &Archiver{}
}

func (a *Archiver) Archive(pictures []*pictures.Picture) (io.Reader, error) {
	buf := new(bytes.Buffer)
	w := zip.NewWriter(buf)
	for _, pic := range pictures {
		f, err := w.Create(pic.Name)
		if err != nil {
			return nil, err // todo wrap
		}
		_, err = f.Write(pic.Body)
		if err != nil {
			return nil, err //todo wrap
		}
	}
	err := w.Close()
	if err != nil {
		return nil, err // todo wrap
	}

	return buf, nil
}
