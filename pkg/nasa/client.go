package nasa

import (
	"context"
	"gitlab.com/gogoapps/kniepok/pictures"
	"golang.org/x/sync/semaphore"
	"net/http"
	"time"
)

type Client struct {
	client http.Client
	apiKey string
}

func NewClient(client http.Client, apiKey string) *Client {
	return &Client{
		client: client,
		apiKey: apiKey,
	}
}

func (c *Client) Fetch(ctx context.Context, timespan pictures.Timespan) ([]*pictures.Picture, error) {
	days := timespan.GetDaysBetween()
	s := semaphore.NewWeighted(5)

	var err error
	results := make(map[time.Time]*pictures.Picture)
	for _, d := range days {
		if err := s.Acquire(ctx, 1); err != nil {
			return nil, err
		}
		go func(day time.Time) {
			defer s.Release(1)
			pic, innerError := c.getPicture(ctx, day)
			if innerError != nil {
				err = innerError
			}
			results[day] = pic
		}(d)
	}

	if err != nil {
		return nil, err
	}

	pics := make([]*pictures.Picture, 0, len(results))
	for _, res := range results {
		pics = append(pics, res)
	}
	return pics, nil
}

func (c *Client) getPicture(ctx context.Context, day time.Time) (*pictures.Picture, error) {
	// todo
	return nil, nil
}
