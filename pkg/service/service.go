package service

import (
	"context"
	"gitlab.com/gogoapps/kniepok/pictures"
	"io"
)

type Service struct {
	fetcher  pictures.Fetcher
	archiver pictures.Archiver
}

func (s *Service) GetPackedPictures(ctx context.Context, timespan pictures.Timespan) (io.Reader, error) {
	fetched, err := s.fetcher.Fetch(ctx, timespan)
	if err != nil {
		return nil, err //todo wrap
	}

	archived, err := s.archiver.Archive(fetched)
	if err != nil {
		return nil, err //todo wrap
	}
	return archived, nil
}
