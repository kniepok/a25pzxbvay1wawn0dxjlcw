package pictures

import (
	"context"
	"io"
	"time"
)

type Timespan struct {
	From, To time.Time
}

func (t Timespan) GetDaysBetween() []time.Time {
	const day = time.Hour * 24
	days := make([]time.Time, 0)
	for {
		if t.From.After(t.To) || t.From.Equal(t.To) {
			return days
		}
		t.From = t.From.Add(day)
		days = append(days, t.From)
	}
}

type Picture struct {
	Name string
	Body []byte
}

type Fetcher interface {
	Fetch(ctx context.Context, timespan Timespan) ([]*Picture, error)
}

type Archiver interface {
	Archive([]*Picture) (io.Reader, error)
}

type Service interface {
	GetPackedPictures(ctx context.Context, timespan Timespan) (archive io.Reader, err error)
}
