
test:
	GO111MODULE=on go test -mod=vendor -v -race ./...


deps:
	export GO111MODULE=on && go mod vendor
